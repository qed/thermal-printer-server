# Thermal printer server

A simple web application that provides a web interface for a thermal printer.

## An installation guide

1. Allow the user that will run the server to use the thermal printer USB
device, for instance using the udev rules provided under
`misc/99-thermal-printer.rules`.
2. Install system dependencies -- for Debian-based distros the following
packages: `libjpeg-dev`, `python3-dev`, `python3-venv`, `libjpeg-dev`.
3. Create a virtual environment with `python3 -m venv env` (where the name
`env` is used in the run script that will show up later) and activate it
vith `source venv/bin/activate`.
4. Install the Python dependencies with `pip install -r requirements.txt`.
5. Run the script `run.sh` or deploy the systemd service under
`misc/printer_server.service`.
