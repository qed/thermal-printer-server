import datetime
import os
import random # TODO: Use secrets.
import string

import escpos.printer as ep
import urllib.parse as up

from flask import Flask, flash, render_template, redirect, request, url_for
from werkzeug.utils import secure_filename

def create_app(config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config['SECRET_KEY'] = ''.join(
          random.choice(string.ascii_lowercase) for i in range(128)
    )

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def home():
        return render_template('index.html')

    @app.route('/text', methods = ('POST', ))
    def print_text():
        if request.method == 'POST':
            text = request.form['text']
            # Below is a hack for printing the letters of the (federal) German
            # alphabet correctly.
            to_print = b''
            table = {
              'Ä': b'\xC4',
              'ä': b'\xE4',
              'Ö': b'\xD6',
              'ö': b'\xF6',
              'Ü': b'\xDC',
              'ü': b'\xFC',
              'ß': b'\xDF'
            }
            for char in text:
                if ord(char) < 128:
                    to_print += char.encode('ascii')
                else:
                    to_print += table.get(char, b'\xA4')
            try:
                printer = ep.Usb(idVendor=0x0416,idProduct=0x5011)
                #printer = ep.Dummy()
                printer._raw(b'\x1B\x40')     # Initialization
                printer._raw(b'\x1B\x74\x06') # Codepage setting
                printer._raw(to_print)
                printer.text('\n\n\n\n\n')
                #flash(printer.output, 'blue')
                printer.close()
            except:
                flash('Could not print.', 'red')
            return redirect(url_for('home'))

    @app.route('/image', methods = ('POST', ))
    def print_file():
        if request.method == 'POST':
            timestamp = '{0:%Y-%m-%d-%H-%M-%S}'.format(datetime.datetime.now())
            salt = ''.join(
                random.choice(string.ascii_lowercase) for i in range(16)
            )
            file_name = timestamp + '_' + salt
            request_file = request.files['image']
            request_filename = secure_filename(request_file.filename)
            split_request_filename = request_filename.rsplit('.', 1)
            if len(split_request_filename) > 1:
                file_name += '.' + split_request_filename[1]
            file_path = os.path.join(app.instance_path, file_name)
            request_file.save(file_path)
            try:
                printer = ep.Usb(idVendor=0x0416,idProduct=0x5011)
                #printer = ep.Dummy()
                try:
                    printer.image(file_path)
                    printer.text('\n\n\n\n\n')
                except IOError:
                    flash('Is this really an image?', 'red')
                #flash(printer.output, 'blue')
                printer.close()
            except:
                flash('Could not print.', 'red')
            os.remove(file_path)
            return redirect(url_for('home'))

    @app.route('/raw', methods = ('POST', ))
    def print_raw():
        if request.method == 'POST':
            try:
                printer = ep.Usb(idVendor=0x0416,idProduct=0x5011)
                #printer = ep.Dummy()
                printer._raw(request.files['raw'].read())
                printer.text('\n\n\n\n\n')
                #flash(printer.output, 'blue')
                printer.close()
            except:
                flash('Could not print.', 'red')
            return redirect(url_for('home'))

    app.add_url_rule('/', endpoint='index')

    return app
