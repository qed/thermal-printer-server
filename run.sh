#!/bin/bash

source env/bin/activate
export FLASK_APP=printer_server
export FLASK_ENV=development
export FLASK_RUN_HOST=0.0.0.0
export FLASK_RUN_PORT=2222

flask run
